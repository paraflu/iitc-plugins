// ==UserScript==
// @id             iitc-plugin-heat-maps-activity@impleri
// @name           IITC plugin: Player Activity Heat Map
// @category       Layer
// @version        1.0.2
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-activity.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-activity.user.js
// @description    Generates a heat map based on player activity. Requires heat-maps and player-tracker plugin.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

;