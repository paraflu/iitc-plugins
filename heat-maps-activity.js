// ==UserScript==
// @id             iitc-plugin-heat-maps-activity@impleri
// @name           IITC plugin: Player Activity Heat Map
// @category       Layer
// @version        1.0.1
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-activity.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-activity.user.js
// @description    Generates a heat map based on player activity. Requires heat-maps and player-tracker plugin.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

;function wrapper(plugin_info) {
  // ensure plugin framework is there, even if iitc is not yet loaded
  if(typeof window.plugin !== 'function') {
    window.plugin = function() {};
  }

  window.plugin.activityHeatMap = function() {};

  window.plugin.activityHeatMap.enlGradient = {
    0.4: 'lightgreen',
    0.6: 'limegreen',
    0.7: 'green',
    1.0: 'darkgreen'
  };

  window.plugin.activityHeatMap.resGradient = {
    0.4: 'lightblue',
    0.6: 'mediumblue',
    0.7: 'blue',
    1.0: 'navy'
  };

  window.plugin.activityHeatMap.start = function() {
    if (window.plugin.playerTracker === undefined) {
       alert("'Activity Heat Map' requires 'player-tracker'");
       return;
    }

    window.plugin.heatMap.create("ENL Activity Heat Map", {gradient: window.plugin.activityHeatMap.enlGradient}, window.plugin.activityHeatMap.getData(TEAM_ENL));
    window.plugin.heatMap.create("RES Activity Heat Map", {gradient: window.plugin.activityHeatMap.resGradient}, window.plugin.activityHeatMap.getData(TEAM_RES));
  }

  window.plugin.activityHeatMap.getData = function(team) {
    return function() {
      var displayBounds = map.getBounds(),
          dataToMap = [];

      $.each(window.plugin.playerTracker.stored, function(name, player) {
        if (getTeam(player) !== team) {
          return true; // continue the loop
        }

        $.each(player.events, function(idx, event) {
          var data = event.latlngs[0];
          if (!displayBounds.contains(data)) {
            return true; // continue the loop
          }

          data.push(0.1);

          dataToMap.push(data);
        });
      });

      return dataToMap;
    };
  }

  var setup = function() {
    window.pluginCreateHook('heatMapLoaded');
    window.addHook('heatMapLoaded', window.plugin.activityHeatMap.start);
  }

  setup.info = plugin_info;
  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  // if IITC has already booted, immediately run the 'setup' function
  if(window.iitcLoaded && typeof setup === 'function') {
    setup();
  }
}

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


