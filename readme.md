# IITC Plugins

## Heat Maps

The Heat Maps Library plugin provides a framework for rendering heat map layers. It's rather boring by itself.

## AP Heat Map

Using your account's team, it generates a relative heat map of AP-rich areas.

## Player Activity Heat Map

Two layers, one for each team, to indicate activity hot spots.
