// ==UserScript==
// @id             iitc-plugin-quick-info@impleri
// @name           IITC plugin: Quick Info
// @category       Tweaks
// @version        0.4.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/quick-info.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/quick-info.user.js
// @description    Show portal information on the map
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==
