// ==UserScript==
// @id             iitc-plugin-quick-info@impleri
// @name           IITC plugin: Quick Info
// @category       Tweaks
// @version        0.4.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/quick-info.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/quick-info.user.js
// @description    Show portal information on the map
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

/*jslint es5: true */
function wrapper(plugin_info) {
  "use strict";

  if (typeof window.plugin !== "function") {
    window.plugin = function() {};
  }

  function QuickInfo (marker) {
    this.marker = marker;
    this.portal = marker.options;
    this.portalDetails = portalDetail.get(marker.options.guid);
    this.output = "";
  }

  QuickInfo.prototype.getLabel = function() {
    var title = "[Unknown]",
        titleClass = "",
        team = getTeam(this.portal.data);

    if (this.portal.data.title) {
      title = this.portal.data.title + " (" + this.portal.level + ")";
    }

    if (team === TEAM_RES) {
      titleClass = "res";
    } else if (team === TEAM_ENL) {
      titleClass = "enl";
    }

    $(document).off("click", ".leaflet-popup-content h3");
    $(document).on("click", ".leaflet-popup-content h3", this.copyInfo(this.portal.data.title, false));

    return "<h3 class=\"" + titleClass + "\">" + title + "</h3>";
  };

  QuickInfo.prototype.getMitigation = function() {
    var linkCount = getPortalLinksCount(this.portal.guid),
        mitigation = getMitigationText(this.portalDetails, linkCount);

    return "<div class=\"quick-right\">" + mitigation[1] + "</div>";
  };

  QuickInfo.prototype.getModDetails = function() {
    var mods = "";

    $.each(this.portalDetails.mods, function(index, mod) {
      var label = "",
          classes = "";

      if (mod) {
        switch(mod.rarity) {
          case "COMMON":
            label = "C";
            classes += " common";
            break;

          case "RARE":
            label = "R";
            classes += " rare";
            break;

          case "VERY_RARE":
            label = "VR";
            classes += " very-rare";
            break;
        }

        switch(mod.name) {
          case "AXA Shield":
            label = "AXA";
            break;

          case "SoftBank Ultra Link":
            label = "SBUL";
            break;

          case "Turret":
            label = "TU";
            break;

          case "Force Amp":
            label = "FA";
            break;

          case "Portal Shield":
            label += "PS";
            break;

          case "Heat Sink":
            label += "HS";
            break;

          case "Multi-hack":
            label += "MH";
            break;

          case "Link Amp":
            label += "LA";
        }
      }

      mods += "<div class=\"quick-bold quick-mod" + classes + "\">" + label + "</div>";
    });

    return mods;
  };

  QuickInfo.prototype.getResoDetails = function() {
    var resos = "";

    $.each(this.portalDetails.resonators, function(index, resonator) {
      var classes = "",
          label = "";
      if (resonator.level > 0) {
        classes += " l" + resonator.level;

        if (resonator.level < 3) {
          classes += " low";
        }

        label = resonator.level;
      }

      resos += "<div class=\"quick-bold quick-reso" + classes + "\">" + label + "</div>";
    });

    return resos;
  };

  QuickInfo.prototype.getEnergy = function() {
    var energy = getEnergyText(this.portalDetails);

    return "<div class=\"quick-right\">" + energy[1] + "</div>";
  };

  QuickInfo.prototype.getApGain = function() {
    var resCount = this.portalDetails.resonators.length;
    var apGain = (8 - resCount) * DEPLOY_RESONATOR;

    if (apGain > 0) {
      apGain += COMPLETION_BONUS;
    }

    if (getTeam(this.portal.data) !== getTeam(PLAYER)) {
      apGain += resCount * DESTROY_RESONATOR;
      apGain += CAPTURE_PORTAL;

      apGain += getPortalLinksCount(this.portal.guid) * DESTROY_LINK;
      apGain += getPortalFieldsCount(this.portal.guid) * DESTROY_FIELD;
    }

    return "<div class=\"quick-third\">AP: " + apGain + "</div>";
  };

  QuickInfo.prototype.getFieldCount = function() {
    var fields = getPortalFieldsCount(this.portal.guid);
    return "<div class=\"quick-third\">Fields: " + fields + "</div>";
  };

  QuickInfo.prototype.getLinkCount = function() {
    var links = getPortalLinks(this.portal.guid);
    return "<div class=\"quick-third\">Links: " + links.out.length + " out / " + links.in.length + " in</div>";
  };

  QuickInfo.prototype.makeClipboardLink = function(label, url) {
    $(document).off("click", ".quick-copy." + label.toLowerCase());
    $(document).on("click", ".quick-copy." + label.toLowerCase(), this.copyInfo(url, true));
    return "<a class=\"quick-copy " + label.toLowerCase() + "\" href=\"" + url + "\">[" + label + "]</a>";
  };

  QuickInfo.prototype.copyInfo = function(data, preventDefault) {
    if (preventDefault === null) {
      preventDefault = false;
    }

    return function(event) {
      if (preventDefault) {
        event.preventDefault();
      }

      var notice = $("<div class=\"quick-notification\">Information Copied</div>");
      $("body").append("<textarea class=\"info-copy\">" + data + "</textarea>");
      $("textarea.info-copy").select();
      document.execCommand("copy");
      $("textarea.info-copy").remove();
      notice.appendTo("body")
        .fadeIn(400)
        .delay(3000)
        .fadeOut(400, function() {
          notice.remove();
        });
    };
  };

  QuickInfo.prototype.getClipboard = function() {
    var lat = this.marker.getLatLng().lat,
        lng = this.marker.getLatLng().lng,
        latlng =  lat + "," + lng,
        intelLink = "https://www.ingress.com/intel?ll=" + latlng + "&z=17&pll=" + latlng,
        appleLink = "https://maps.apple.com/?q=" + latlng,
        bingLink = "https://www.bing.com/maps/?v=2&cp=" + lat + "~"
          + lng + "&lvl=16&sp=Point." + lat + "_"
          + lng + "_" + this.portal.data.title + "___",
        gmapsLink = "https://maps.google.com/?q=" + latlng;

    $(document).off("click", ".quick-copy.guid");
    $(document).on("click", ".quick-copy.guid", this.copyInfo(this.portal.guid));

    return "<div class=\"clearfix quick-row small\"><span class=\"copy-icon\"></span>"
      + this.makeClipboardLink("Intel", intelLink)
      + this.makeClipboardLink("Google", gmapsLink)
      + this.makeClipboardLink("Apple", appleLink)
      + this.makeClipboardLink("Bing", bingLink)
      + "<span class=\"quick-copy guid\">[GUID]</span>"
      + "</div>";
  };

  QuickInfo.prototype.getDetailedInfo = function() {
    this.output += "<div class=\"clearfix quick-row quick-center\">" + this.getResoDetails() + this.getEnergy() + "</div>";
    this.output += "<div class=\"clearfix quick-row\">" + this.getModDetails() + this.getMitigation() + "</div>";
    this.output += "<div class=\"clearfix quick-row small\">" + this.getLinkCount() + this.getFieldCount() + this.getApGain() + "</div>";
    this.output += this.getClipboard();
  };

  QuickInfo.prototype.getBasicInfo = function () {
    this.output += this.getLabel();
  };

  QuickInfo.prototype.render = function () {
    this.getBasicInfo();

    if (this.portalDetails) {
      this.getDetailedInfo();
    }

    return this.output;
  };

  var setup = function() {
    addHook("portalDetailsUpdated", function(data) {
      var portal = data.portal,
          info = new QuickInfo(portal),
          popup = L.popup({
            autoPan: false,
            className: "quick-info-popup"
          });

      popup.setContent(info.render());
      portal.bindPopup(popup).openPopup();
    });
  };

  setup.info = {};
  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  if(window.iitcLoaded && typeof setup === "function") {
    setup();
  }
}

var script = document.createElement("script"),
    css = document.createElement('style'),
    info = {};

if (typeof GM_info !== "undefined" && GM_info && GM_info.script) {
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}

script.appendChild(document.createTextNode("(" + wrapper + ")(" + JSON.stringify(info) + ");"));
(document.body || document.head || document.documentElement).appendChild(script);

var copyIcon = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDU2MSA1NjEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU2MSA1NjE7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8ZyBpZD0iY29udGVudC1jb3B5Ij4KCQk8cGF0aCBkPSJNMzk1LjI1LDBoLTMwNmMtMjguMDUsMC01MSwyMi45NS01MSw1MXYzNTdoNTFWNTFoMzA2VjB6IE00NzEuNzUsMTAyaC0yODAuNWMtMjguMDUsMC01MSwyMi45NS01MSw1MXYzNTcgICAgYzAsMjguMDUsMjIuOTUsNTEsNTEsNTFoMjgwLjVjMjguMDUsMCw1MS0yMi45NSw1MS01MVYxNTNDNTIyLjc1LDEyNC45NSw0OTkuOCwxMDIsNDcxLjc1LDEwMnogTTQ3MS43NSw1MTBoLTI4MC41VjE1M2gyODAuNVY1MTAgICAgeiIgZmlsbD0iI0ZGREE0NCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=";

css.type = "text/css";
css.innerHTML = '\
  .clearfix:after {\
    content: "";\
    display: table;\
    clear: both;\
  }\
  .quick-bold {\
    font-weight: bold;\
  }\
  .quick-center {\
    text-align: center;\
  }\
  .quick-right {\
    float: right;\
    width: 74px;\
  }\
  .quick-row {\
    font-size: 1rem;\
    line-height: 1.25;\
    margin-bottom: 2px;\
    width: 290px;\
  }\
  .quick-row.small {\
    font-size: 0.9rem;\
  }\
  .quick-third {\
    color: ' + COLORS_MOD["COMMON"] + ';\
    float: left;\
    margin: 0 2px;\
    width: 70px;\
  }\
  .quick-third:first-child {\
    width: 130px;\
  }\
  .quick-reso {\
    color: #FFFFFF;\
    float: left;\
    font-size: 0.9rem;\
    margin: 0 1px;\
    width: 23px;\
    text-align: center;\
  }\
  .quick-mod {\
    border: 1px solid gray;\
    float: left;\
    font-size: 0.9rem;\
    margin: 0 1.5px;\
    min-height: 17px;\
    text-align: center;\
    width: 45px;\
  }\
  .quick-reso.low {\
    color: #9900FF;\
  }\
  .quick-reso.l1 {\
    background-color: ' + COLORS_LVL[1] + ';\
  }\
  .quick-reso.l2 {\
    background-color: ' + COLORS_LVL[2] + ';\
  }\
  .quick-reso.l3 {\
    background-color: ' + COLORS_LVL[3] + ';\
  }\
  .quick-reso.l4 {\
    background-color: ' + COLORS_LVL[4] + ';\
  }\
  .quick-reso.l5 {\
    background-color: ' + COLORS_LVL[5] + ';\
  }\
  .quick-reso.l6 {\
    background-color: ' + COLORS_LVL[6] + ';\
  }\
  .quick-reso.l7 {\
    background-color: ' + COLORS_LVL[7] + ';\
  }\
  .quick-reso.l8 {\
    background-color: ' + COLORS_LVL[8] + ';\
  }\
  .quick-mod.common {\
    color: ' + COLORS_MOD["COMMON"] + ';\
  }\
  .quick-mod.rare {\
    color: ' + COLORS_MOD["RARE"] + ';\
  }\
  .quick-mod.very-rare {\
    color: ' + COLORS_MOD["VERY_RARE"] + ';\
  }\
  .copy-icon {\
    background: url(' + copyIcon + ') no-repeat;\
    max-height: 20px;\
    max-width: 20px;\
    padding-right: 20px;\
  }\
  .leaflet-popup-content h3 {\
    cursor: pointer;\
  }\
  .leaflet-container .quick-row .quick-copy {\
    color: ' + COLORS_MOD["RARE"] + ';\
    cursor: pointer;\
    display: inline-block;\
    margin: 0 5px;\
  }\
  .quick-notification {\
    width: 200px;\
    height: 20px;\
    height: auto;\
    position: absolute;\
    left: 50%;\
    margin-left: -100px;\
    top: 20px;\
    z-index: 10000;\
    background-color: #383838;\
    color: #F0F0F0;\
    font-family: Calibri;\
    font-size: 20px;\
    padding: 10px;\
    text-align: center;\
    border-radius: 2px;\
    box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);\
  }\
';

document.head.appendChild(css);
