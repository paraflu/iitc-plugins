// ==UserScript==
// @id             iitc-plugin-anomaly-export@impleri
// @name           IITC plugin: Anomaly Export
// @category       Tweaks
// @version        0.2.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/anomaly-export.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/anomaly-export.user.js
// @description    Exports loaded portals within drawn polygons as CSV.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==
