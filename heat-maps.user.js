// ==UserScript==
// @id             iitc-plugin-heat-maps@impleri
// @name           IITC plugin: Heat maps Library
// @category       Misc
// @version        1.0.2
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps.user.js
// @description    Heat Maps library. Doesn't make anything on its own.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

;'use strict';

if (typeof module !== 'undefined') module.exports = simpleheat;

function simpleheat(canvas) {
    if (!(this instanceof simpleheat)) return new simpleheat(canvas);

    this._canvas = canvas = typeof canvas === 'string' ? document.getElementById(canvas) : canvas;

    this._ctx = canvas.getContext('2d');
    this._width = canvas.width;
    this._height = canvas.height;

    this._max = 1;
    this._data = [];
}

simpleheat.prototype = {

    defaultRadius: 25,

    defaultGradient: {
        0.4: 'blue',
        0.6: 'cyan',
        0.7: 'lime',
        0.8: 'yellow',
        1.0: 'red'
    },

    data: function (data) {
        this._data = data;
        return this;
    },

    max: function (max) {
        this._max = max;
        return this;
    },

    add: function (point) {
        this._data.push(point);
        return this;
    },

    clear: function () {
        this._data = [];
        return this;
    },

    radius: function (r, blur) {
        blur = blur === undefined ? 15 : blur;

        // create a grayscale blurred circle image that we'll use for drawing points
        var circle = this._circle = document.createElement('canvas'),
            ctx = circle.getContext('2d'),
            r2 = this._r = r + blur;

        circle.width = circle.height = r2 * 2;

        ctx.shadowOffsetX = ctx.shadowOffsetY = r2 * 2;
        ctx.shadowBlur = blur;
        ctx.shadowColor = 'black';

        ctx.beginPath();
        ctx.arc(-r2, -r2, r, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();

        return this;
    },

    resize: function () {
        this._width = this._canvas.width;
        this._height = this._canvas.height;
    },

    gradient: function (grad) {
        // create a 256x1 gradient that we'll use to turn a grayscale heatmap into a colored one
        var canvas = document.createElement('canvas'),
            ctx = canvas.getContext('2d'),
            gradient = ctx.createLinearGradient(0, 0, 0, 256);

        canvas.width = 1;
        canvas.height = 256;

        for (var i in grad) {
            gradient.addColorStop(i, grad[i]);
        }

        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, 1, 256);

        this._grad = ctx.getImageData(0, 0, 1, 256).data;

        return this;
    },

    draw: function (minOpacity) {
        if (!this._circle) this.radius(this.defaultRadius);
        if (!this._grad) this.gradient(this.defaultGradient);

        var ctx = this._ctx;

        ctx.clearRect(0, 0, this._width, this._height);

        // draw a grayscale heatmap by putting a blurred circle at each data point
        for (var i = 0, len = this._data.length, p; i < len; i++) {
            p = this._data[i];
            ctx.globalAlpha = Math.max(p[2] / this._max, minOpacity === undefined ? 0.05 : minOpacity);
            ctx.drawImage(this._circle, p[0] - this._r, p[1] - this._r);
        }

        // colorize the heatmap, using opacity value of each pixel to get the right color from our gradient
        var colored = ctx.getImageData(0, 0, this._width, this._height);
        this._colorize(colored.data, this._grad);
        ctx.putImageData(colored, 0, 0);

        return this;
    },

    _colorize: function (pixels, gradient) {
        for (var i = 0, len = pixels.length, j; i < len; i += 4) {
            j = pixels[i + 3] * 4; // get gradient color from opacity value

            if (j) {
                pixels[i] = gradient[j];
                pixels[i + 1] = gradient[j + 1];
                pixels[i + 2] = gradient[j + 2];
            }
        }
    }
};

/*
 (c) 2014, Vladimir Agafonkin
 Leaflet.heat, a tiny and fast heatmap plugin for Leaflet.
 https://github.com/Leaflet/Leaflet.heat
*/

L.HeatLayer = (L.Layer ? L.Layer : L.Class).extend({

    // options: {
    //     minOpacity: 0.05,
    //     maxZoom: 18,
    //     radius: 25,
    //     blur: 15,
    //     max: 1.0
    // },

    initialize: function (latlngs, options) {
        this._latlngs = latlngs;
        L.setOptions(this, options);
    },

    setLatLngs: function (latlngs) {
        this._latlngs = latlngs;
        return this.redraw();
    },

    addLatLng: function (latlng) {
        this._latlngs.push(latlng);
        return this.redraw();
    },

    setOptions: function (options) {
        L.setOptions(this, options);
        if (this._heat) {
            this._updateOptions();
        }
        return this.redraw();
    },

    redraw: function () {
        if (this._heat && !this._frame && !this._map._animating) {
            this._frame = L.Util.requestAnimFrame(this._redraw, this);
        }
        return this;
    },

    onAdd: function (map) {
        this._map = map;

        if (!this._canvas) {
            this._initCanvas();
        }

        map._panes.overlayPane.appendChild(this._canvas);

        map.on('moveend', this._reset, this);

        if (map.options.zoomAnimation && L.Browser.any3d) {
            map.on('zoomanim', this._animateZoom, this);
        }

        this._reset();
    },

    onRemove: function (map) {
        map.getPanes().overlayPane.removeChild(this._canvas);

        map.off('moveend', this._reset, this);

        if (map.options.zoomAnimation) {
            map.off('zoomanim', this._animateZoom, this);
        }
    },

    addTo: function (map) {
        map.addLayer(this);
        return this;
    },

    _initCanvas: function () {
        var canvas = this._canvas = L.DomUtil.create('canvas', 'leaflet-heatmap-layer leaflet-layer');

        var originProp = L.DomUtil.testProp(['transformOrigin', 'WebkitTransformOrigin', 'msTransformOrigin']);
        canvas.style[originProp] = '50% 50%';

        var size = this._map.getSize();
        canvas.width  = size.x;
        canvas.height = size.y;

        var animated = this._map.options.zoomAnimation && L.Browser.any3d;
        L.DomUtil.addClass(canvas, 'leaflet-zoom-' + (animated ? 'animated' : 'hide'));

        this._heat = simpleheat(canvas);
        this._updateOptions();
    },

    _updateOptions: function () {
        this._heat.radius(this.options.radius || this._heat.defaultRadius, this.options.blur);

        if (this.options.gradient) {
            this._heat.gradient(this.options.gradient);
        }
        if (this.options.max) {
            this._heat.max(this.options.max);
        }
    },

    _reset: function () {
        var topLeft = this._map.containerPointToLayerPoint([0, 0]);
        L.DomUtil.setPosition(this._canvas, topLeft);

        var size = this._map.getSize();

        if (this._heat._width !== size.x) {
            this._canvas.width = this._heat._width  = size.x;
        }
        if (this._heat._height !== size.y) {
            this._canvas.height = this._heat._height = size.y;
        }

        this._redraw();
    },

    _redraw: function () {
        var data = [],
            r = this._heat._r,
            size = this._map.getSize(),
            bounds = new L.Bounds(
                L.point([-r, -r]),
                size.add([r, r])),

            max = this.options.max === undefined ? 1 : this.options.max,
            maxZoom = this.options.maxZoom === undefined ? this._map.getMaxZoom() : this.options.maxZoom,
            v = 1 / Math.pow(2, Math.max(0, Math.min(maxZoom - this._map.getZoom(), 12))),
            cellSize = r / 2,
            grid = [],
            panePos = this._map._getMapPanePos(),
            offsetX = panePos.x % cellSize,
            offsetY = panePos.y % cellSize,
            i, len, p, cell, x, y, j, len2, k;

        // console.time('process');
        for (i = 0, len = this._latlngs.length; i < len; i++) {
            p = this._map.latLngToContainerPoint(this._latlngs[i]);
            if (bounds.contains(p)) {
                x = Math.floor((p.x - offsetX) / cellSize) + 2;
                y = Math.floor((p.y - offsetY) / cellSize) + 2;

                var alt =
                    this._latlngs[i].alt !== undefined ? this._latlngs[i].alt :
                    this._latlngs[i][2] !== undefined ? +this._latlngs[i][2] : 1;
                k = alt * v;

                grid[y] = grid[y] || [];
                cell = grid[y][x];

                if (!cell) {
                    grid[y][x] = [p.x, p.y, k];

                } else {
                    cell[0] = (cell[0] * cell[2] + p.x * k) / (cell[2] + k); // x
                    cell[1] = (cell[1] * cell[2] + p.y * k) / (cell[2] + k); // y
                    cell[2] += k; // cumulated intensity value
                }
            }
        }

        for (i = 0, len = grid.length; i < len; i++) {
            if (grid[i]) {
                for (j = 0, len2 = grid[i].length; j < len2; j++) {
                    cell = grid[i][j];
                    if (cell) {
                        data.push([
                            Math.round(cell[0]),
                            Math.round(cell[1]),
                            Math.min(cell[2], max)
                        ]);
                    }
                }
            }
        }
        // console.timeEnd('process');

        // console.time('draw ' + data.length);
        this._heat.data(data).draw(this.options.minOpacity);
        // console.timeEnd('draw ' + data.length);

        this._frame = null;
    },

    _animateZoom: function (e) {
        var scale = this._map.getZoomScale(e.zoom),
            offset = this._map._getCenterOffset(e.center)._multiplyBy(-scale).subtract(this._map._getMapPanePos());

        if (L.DomUtil.setTransform) {
           L.DomUtil.setTransform(this._canvas, offset, scale);

        } else {
            this._canvas.style[L.DomUtil.TRANSFORM] = L.DomUtil.getTranslateString(offset) + ' scale(' + scale + ')';
        }
    }
});

L.heatLayer = function (latlngs, options) {
    return new L.HeatLayer(latlngs, options);
};

function wrapper(plugin_info) {
  // ensure plugin framework is there, even if iitc is not yet loaded
  if(typeof window.plugin !== 'function') {
    window.plugin = function() {};
  }

  /**
   * Heat Map Layer
   *
   * Class constructor for a heat map layer.
   * @param {string}   layerName Unique name to use in Layer menu.
   * @param {object}   options   Options
   * @param {Function} callback  Callback to provide data.
   */
  function HeatMapLayer(layerName, options, callback) {
    if (typeof options === "function") {
      callback = options;
      options = {};
    }

    this.callback = callback;
    this.disabled = true;
    this.initialized = false;
    this.name = layerName;
    this.options = jQuery.extend({}, window.plugin.heatMap.defaultOptions, options);

    this.intialize();
  }

  /**
   * Initialize
   *
   * Adds the layer to the map and binds the object to map and iitc lifecycle events.
   * @return boolean True if successful
   */
  HeatMapLayer.prototype.intialize = function() {
    if (typeof this.callback !== "function") {
      console.log("Heat map requires a callback function to map raw portal data.");
      return false;
    }

    this.layer = L.heatLayer([], this.options);
    window.addLayerGroup(this.name, this.layer);
    this.changedZoom(map.getZoom());

    map.on('layeradd', this.onAddLayer.bind(this));
    map.on('layerremove', this.onRemoveLayer.bind(this));
    window.addHook('mapDataRefreshEnd', this.refresh.bind(this));

    // ensure 'disabled' flag is initialised
    if (map.hasLayer(this.layer)) {
        this.disabled = false;
    }

    this.initialized = true;

    return this.initialized;
  };

  /**
   * On Add Layer
   *
   * Callback fired when a layer is added to the map
   * @param  {object} obj Layer metadata object
   * @return void
   */
  HeatMapLayer.prototype.onAddLayer = function(obj) {
    if(obj.layer === this.layer) {
      this.disabled = false;
      this.refresh();
    }
  };

  /**
   * On Remove Layer
   *
   * Callback fired when a layer is removed from the map
   * @param  {object} obj Layer metadata object
   * @return void
   */
  HeatMapLayer.prototype.onRemoveLayer = function(obj) {
    if(obj.layer === this.layer) {
      this.disabled = true;
      this.clear();
    }
  };

  /**
   * Clear
   *
   * Removes all data from heat map layer
   * @return void
   */
  HeatMapLayer.prototype.clear = function() {
    this.layer.setLatLngs([]);
  };

  /**
   * Refresh
   * @return void
   */
  HeatMapLayer.prototype.refresh = function() {
    if (this.disabled) {
      return;
    }

    var zoomLevel = map.getZoom();

    console.debug(this.name + ": refreshing data");
    this.layer.setLatLngs(this.callback());

    if (zoomLevel !== this.currentZoomLevel) {
      this.changedZoom(zoomLevel);
    }
  };

  HeatMapLayer.prototype.changedZoom = function(zoomLevel) {
    var rawScale = this.options.maxZoom - zoomLevel - 1,
        scale = (rawScale > 0) ? rawScale : 1,
        radius = this.options.radius/scale,
        blur = this.options.blur/scale;

    this.currentZoomLevel = zoomLevel;
    this.layer.setOptions($.extend({}, this.options, {
      radius: (radius > 1) ? radius : 1,
      blur: (blur > 1) ? blur : 1
    }));
  }

  window.plugin.heatMap = function() {};

  window.plugin.heatMap.defaultOptions = {
    minOpacity: 0.2,
    maxZoom: 18,
    max: 1.0,
    radius: 25,
    blur: 15,
    gradient: {
      0.3: 'yellow',
      0.5: 'orange',
      0.7: 'fuchsia',
      1.0: 'purple'
    }
  };

  window.plugin.heatMap.layers = {};

  window.plugin.heatMap.create = function(name, options, callback) {
    if (window.plugin.heatMap.layers[name]) {
      console.log("The layer named " + name + " already exists.");
      return;
    }

    window.plugin.heatMap.layers[name] = new HeatMapLayer(name, options, callback);
    return window.plugin.heatMap.layers[name].initialized;
  };

  var setup =  function() {
    window.pluginCreateHook('heatMapLoaded');
    window.addHook('iitcLoaded', function() {
      window.runHooks('heatMapLoaded');
    });
  };

  setup.info = null;

  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  // if IITC has already booted, immediately run the 'setup' function
  if(window.iitcLoaded) {
    setup();
  }
}

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);

