// ==UserScript==
// @id             iitc-plugin-anomaly-export@impleri
// @name           IITC plugin: Anomaly Export
// @category       Tweaks
// @version        0.2.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/anomaly-export.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/anomaly-export.user.js
// @description    Exports loaded portals within drawn polygons as CSV.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

/*jslint es5: true */
function wrapper(plugin_info) {
  "use strict";

  if (typeof window.plugin !== "function") {
    window.plugin = function() {};
  }


  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
  function exportPolygon(layer) {
    var polygon = [];

    layer.getLatLngs().forEach(function(point) {
      polygon.push([point.lat, point.lng]);
    });

    return function(point) {
      var x = point[0],
          y = point[1],
          inside = false,
          i,
          j;

      for (i=0, j = polygon.length-1; i < polygon.length; j = i++) {
        var xi = polygon[i][0],
            yi = polygon[i][1],
            xj = polygon[j][0],
            yj = polygon[j][1],
            intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);

        if (intersect) {
          inside = !inside;
        }
      }

      return inside;
    };
  }

  // Adapted from Arcs
  function exportCircle(layer) {
     return function(point) {
        var R = 6367, // km
            lat1 = layer.getLatLng().lat,
            lon1 = layer.getLatLng().lng,
            lat2 = point[0],
            lon2 = point[1],
            dLat = (lat2-lat1) * Math.PI / 180,
            dLon = (lon2-lon1) * Math.PI / 180,
            lat1 = lat1 * Math.PI / 180,
            lat2 = lat2 * Math.PI / 180,
            a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return ((R * c * 1000) <= layer.getRadius());
     };
  }

  window.plugin.anomalyExport = {};

  window.plugin.anomalyExport.exports = [];

  /**
   * Control Button
   *
   * @param  {Object} Configuration options
   * @return {void}
   */
  window.plugin.anomalyExport.controlBtn = L.Control.extend({
    options: {
      position: "topleft"
    },

    onAdd: function (map) {
      var container = L.DomUtil.create("div", "leaflet-anomaly-export");

      $(container).append("<div class=\"leaflet-bar anomaly-export\"><a href=\"javascript: void(0);\">AE</a></div>");

      $(container).on("click", function() {
        window.plugin.anomalyExport.generate();
        window.plugin.anomalyExport.dialog();
      });

      return container;
    }
  });

  /**
   * Export Layer
   * @param  Array    portals  Array of points to test
   * @param  Function callback Callback to determine if point is within layer. Parameter given is latlng array and expects a boolean return value
   * @return void
   */
  window.plugin.anomalyExport.exportLayer = function (portals, type, callback) {
    var results = "GUID,Name,Coordinates\n",
        hasProp = Object.prototype.hasOwnProperty,
        index,
        point,
        portal;

    for (index in portals) {
      if (!hasProp.call(portals, index)) {
        continue;
      }

      portal = portals[index];
      point = [
        (portal._latlng != null) ? portal._latlng.lat : 0,
        (portal._latlng != null) ? portal._latlng.lng : 0
      ];

      if (callback(point)) {
        results += '"' + portal.options.guid + '","' + portal.options.data.title + '","' + point.join(',') + '"\n';
      }
    }

    window.plugin.anomalyExport.exports.push({type: type, csv: results});
  };

  window.plugin.anomalyExport.generate = function () {
    window.plugin.anomalyExport.exports = [];

    window.plugin.drawTools.drawnItems.eachLayer(function(layer) {
      var callback,
          type = "Unknown";
     if (layer instanceof L.GeodesicCircle || layer instanceof L.Circle) {
      callback = exportCircle(layer);
      type = "Circle";
     } else if (layer instanceof L.GeodesicPolygon || layer instanceof L.Polygon) {
      callback = exportPolygon(layer);
      type = "Polygon";
     }

     if (callback) {
      window.plugin.anomalyExport.exportLayer(window.portals, type, callback);
     }
    });
  };

  /**
   * Dialog
   *
   * Launch the Anomaly Export dialog
   * @return {void}
   */
  window.plugin.anomalyExport.dialog = function() {
    var html =  "<div id=\"anomaly-export-summary\">";
    html += "<h3>Stats</h3>";
    html += "<p>Found " + window.plugin.anomalyExport.exports.length + " layers.</p>";
    $.each(window.plugin.anomalyExport.exports, function(index, layer) {
      html += "<div class=\"anomaly-export\">";
      html += "<div>" + layer.type + "<button class=\"anomaly-export-copy\" data-id=\"" + index + "\">Copy</button></div>";
      html += "<div><textarea id=\"anomaly-export-" + index + "\" readonly=\"readonly\">" + layer.csv + "</textarea></div>";
      html += "</div>";
    });

    html += "</div>";

    dialog({
      id: "anomaly-export",
      title: "Anomaly Export",
      html: html
    });

    $(".anomaly-export-copy").off("click");
    $(".anomaly-export-copy").on("click", function(event) {
      var notice = $("<div class=\"quick-notification\">Information Copied</div>"),
          index = $(event.target).data("id");

      event.preventDefault();
      $("textarea#anomaly-export-" + index).select();
      document.execCommand("copy");
      notice.appendTo("body")
        .fadeIn(400)
        .delay(3000)
        .fadeOut(400, function() {
          notice.remove();
        });
    });
  };

  window.plugin.anomalyExport.init = function () {
    if (!window.useAndroidPanes()) {
      map.addControl(new window.plugin.anomalyExport.controlBtn());
    }
  };

  function setup() {
    window.addHook('iitcLoaded', window.plugin.anomalyExport.init);
  }

  setup.info = {};
  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  if(window.iitcLoaded && typeof setup === "function") {
    setup();
  }
}

var script = document.createElement("script"),
    info = {};

if (typeof GM_info !== "undefined" && GM_info && GM_info.script) {
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}

script.appendChild(document.createTextNode("(" + wrapper + ")(" + JSON.stringify(info) + ");"));
(document.body || document.head || document.documentElement).appendChild(script);
