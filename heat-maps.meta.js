// ==UserScript==
// @id             iitc-plugin-heat-maps@impleri
// @name           IITC plugin: Heat maps Library
// @category       Misc
// @version        1.0.2
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps.user.js
// @description    Heat Maps library. Doesn't make anything on its own.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

;