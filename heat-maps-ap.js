// ==UserScript==
// @id             iitc-plugin-heat-maps-ap@impleri
// @name           IITC plugin: AP Heat Map
// @category       Layer
// @version        1.0.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-ap.meta.js
// @downloadURL    https://gitlab.com/enl-intel/iitc-plugins/blob/master/heat-maps-ap.user.js
// @description    Generates a heat map based on AP gain. Requires heat-maps plugin.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==

;function wrapper(plugin_info) {
  // ensure plugin framework is there, even if iitc is not yet loaded
  if(typeof window.plugin !== 'function') {
    window.plugin = function() {};
  }

  window.plugin.apHeatMap = function() {};

  window.plugin.apHeatMap.start = function() {
    window.plugin.heatMap.create("AP Heat Map", {}, window.plugin.apHeatMap.getData);
  }

  window.plugin.apHeatMap.getData = function() {
    var displayBounds = map.getBounds(),
        dataToMap = [],
        portals = [],
        maxAp = 0;

    $.each(window.portals, function(guid, portal) {
      var latlng = portal.getLatLng();
      if (!displayBounds.contains(latlng)) {
        return true; // continue the loop
      }

      var apGain = window.plugin.apHeatMap.calculateApGain(guid, portal.options.team, portal.options.data.resCount);

      if (apGain > maxAp) {
        maxAp = apGain;
      }

      portals.push({
        apGain: apGain,
        entry: [latlng.lat, latlng.lng]
      });
    });

    $.each(portals, function(index, portal) {
      portal.entry.push(portal.apGain / maxAp);
      dataToMap.push(portal.entry);
    });

    return dataToMap;
  }

  window.plugin.apHeatMap.calculateApGain = function(guid, team, resCount) {
    resCount = resCount || 0;
    var apGain = (8 - resCount) * DEPLOY_RESONATOR;

    if (apGain > 0) {
      apGain += COMPLETION_BONUS;
    }

    if (team !== getTeam(PLAYER)) {
      apGain += resCount * DESTROY_RESONATOR;
      apGain += CAPTURE_PORTAL;

      apGain += getPortalLinksCount(guid) * DESTROY_LINK;
      apGain += getPortalFieldsCount(guid) * DESTROY_FIELD;
    }

    return apGain;
  }

  var setup = function() {
    window.pluginCreateHook('heatMapLoaded');
    window.addHook('heatMapLoaded', window.plugin.apHeatMap.start);
  }

  setup.info = plugin_info;
  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  // if IITC has already booted, immediately run the 'setup' function
  if(window.iitcLoaded && typeof setup === 'function') {
    setup();
  }
}

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


